MANS = src/usr/share/man/man1/xynagios.1 src/usr/share/man/man3/Hobbit.3pm

all: $(MANS)

src/usr/share/man/man1/xynagios.1: src/usr/bin/xynagios
	mkdir -p $(dir $@)
	pod2man $< > $@

src/usr/share/man/man3/Hobbit.3pm: src/usr/share/perl5/Hobbit.pm
	mkdir -p $(dir $@)
	pod2man --section=3pm $< > $@

install: $(MANS)
	rsync -ar src/ $(DESTDIR)/

clean:
	rm -f $(MANS)

test:
	prove -v -l
